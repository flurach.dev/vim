" Plugins
call plug#begin(stdpath('data') . '/plugged')
        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'
        Plug 'lxmzhv/vim'

        Plug 'machakann/vim-sandwich'
        Plug 'ctrlpvim/ctrlp.vim'
        Plug 'bronson/vim-trailing-whitespace'
call plug#end()

" Styling
colorscheme mizore
let g:airline_powerline_fonts = 1

" Config
let mapleader = " "
set cc=80
set number
set noshowmode
set noshowcmd
set shortmess+=F
set splitright splitbelow
hi ColorColumn ctermbg=lightgrey guibg=lightgrey

" Bindings
nnoremap <leader>w      :w<cr>
nnoremap <leader>q      :q<cr>
